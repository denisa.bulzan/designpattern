import java.util.Scanner;

public class Main {
    public static void main(String args[]) {

        Scanner in = new Scanner(System.in);
        System.out.println("Insert page name");
        String input = in.nextLine();

        Page page = new Page().name("About", input);
        System.out.println(page.getDetails());

    }
}