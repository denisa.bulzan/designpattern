public abstract class Page extends Themes{
    public String name;

    protected Page(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }

    public String getDetails(){
        return "This page is:" + name;
    }
    public showTheme(String Name){
        if (Name.equals("Dark")) {
            System.out.println("DarkTheme was created");
        } else if (Name.equals("Light")) {
            System.out.println("LightTheme was created");

        } else if (Name.equals("Aqua"))
            System.out.println("AquaTheme was created");
    }
}