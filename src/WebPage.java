public abstract class WebPage {
    public abstract Page createPage(String pageName);
}