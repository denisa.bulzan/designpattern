public class NewPage{
    @Override
    public Page createPage(String pageType) {
        if (pageType.equals("About")) {
            return new About();
        }else if (pageType.equals("Careers")) {
            return new Careers();

        } else if (pageType.equals("Projects")){
            return new Projects();
        }
        return null;
    }
}